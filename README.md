## Introducción a programación en R

- [Primera sesión en R](intro/05-primera-sesion/primera-sesion.md)
- [Funciones básicas](intro/01-funciones-basicas/funciones-basicas.md)
- [Guía de estilo en R](intro/03-guia-de-estilo/guia-de-estilo.md)
- [Sintaxis básica](intro/04-sintaxis-basica/sintaxis-basica.md)
- [Tipos de datos](intro/02-tipos-de-datos/tipos-de-datos.md)
- [Vectores](intro/06-vectores/vectores.md)

## Iniciación

- ["R para principiantes - Introducción e instalación"](https://soka.gitlab.io/blog/post/2019-01-27-introduccion-r/).
- ["Operadores y aritmética básica en R - Aritméticos, comparativos y lógicos"](https://soka.gitlab.io/blog/post/2019-01-28-aritmetica-y-objetos-en-r/).
- ["Paquetes en R - Como intalarlos y usarlos"](https://soka.gitlab.io/blog/post/2019-01-30-paquetes-en-r/).
- ["Funciones básicas en R - mean() y sum()"](https://soka.gitlab.io/blog/post/2019-02-27-funciones-basicas-mean-sum-en-r/).

## Tipos de datos, objetos y variables

- ["Tipos de datos básicos en R"](https://soka.gitlab.io/blog/post/2019-03-03-tipos-de-datos-r/).
- ["Manipulación de cadenas en R"](https://soka.gitlab.io/blog/post/2019-08-11-r-manipulacion-cadenas/).
- ["Vectores"](https://soka.gitlab.io/blog/post/2019-03-11-r-vectores/).
- ["Vectores y funciones"](https://soka.gitlab.io/blog/post/2019-08-21-r-vectores-y-funciones/).
- ["Familia apply"](https://soka.gitlab.io/blog/post/2019-08-11-r-apply-funciones-a-objetos/): Aplicar una función sobre estructuras de datos.
- ["Matrices"](https://soka.gitlab.io/blog/post/2019-03-11-r-matrices/).
- ["Listas"](https://soka.gitlab.io/blog/post/2019-03-11-r-listas/).
- ["Data.Frame"](https://soka.gitlab.io/blog/post/2019-03-12-r-data-frame/).
- Data.frame ["Añadir una nueva columna"](https://soka.gitlab.io/blog/post/2019-03-01-ainadir-nueva-columna-df-en-r/).
- ["Objetos en R - Propiedades y conversión de tipo"](https://soka.gitlab.io/blog/post/2019-03-14-r-conversi%C3%B3n-de-objetos/).
- ["Seleccionar datos con [] y subset"](https://soka.gitlab.io/blog/post/2019-03-15-r-seleecionar-datos-subset/).
- ["Operaciones con Data Frames"](https://soka.gitlab.io/blog/post/2019-04-22-r-operaciones-con-data-frames/).
- ["Factores"](https://soka.gitlab.io/blog/post/2019-08-15-r-factores/).
- ["Funciones"](https://soka.gitlab.io/blog/post/2019-03-15-r-funciones-intro/).
- ["Bucles"](https://soka.gitlab.io/blog/post/2019-03-17-r-bucles/).
- ["Objetos ts Series de tiempo"](https://soka.gitlab.io/blog/post/2019-04-16-r-series-de-tiempo/).
- ["Estructuras de datos en R"](https://soka.gitlab.io/blog/post/2019-05-10-r-resumen-estrucuturas-de-datos/).
- ["Happy programmers day!"](https://soka.gitlab.io/blog/post/2019-09-13-happy-programmers-day/): Un excusa para jugar con fechas en R.

## Importar-exportar ficheros en diferentes formatos

- ["Descarga de un fichero de Internet"](https://soka.gitlab.io/blog/post/2019-04-22-r-descarga-fichero-internet/).
- ["Importar datos de un CSV - Data frames en R"](https://soka.gitlab.io/blog/post/2019-01-29-importar-datos-csv-en-r/).
- ["Importar datos XLSX (Excel)"](https://soka.gitlab.io/blog/post/2019-01-31-importar-datos-excel-xlsx/).
- ["Importar datos XML en R"](https://soka.gitlab.io/blog/post/2019-02-05-importar-datos-xml/).
- ["Importar datos en R - JSON"](https://soka.gitlab.io/blog/post/2019-02-26-trabajando-con-datos-json-en-r/).
- ["Exportar los datos a un fichero CSV"](https://soka.gitlab.io/blog/post/2019-03-01-exportar-los-datos-r/).

## Funciones

- ["Funciones en R - Introducción"](https://soka.gitlab.io/blog/post/2019-03-15-r-funciones-intro/).

## Algoritmos

- ["Ordenación Bubble sort"](https://soka.gitlab.io/blog/post/2019-09-12-r-bubble-sort/).

## Diagramas

- ["R - Gráficos básicos - Función plot()"](https://soka.gitlab.io/blog/post/2019-03-18-r-graficos-basicos-plot/).
- ["Diagramas de dispersión y modelo de regresión líneal en R"](https://soka.gitlab.io/blog/post/2019-02-05-diagramas-dispersion-en-r/).
- ["Diagramas tipo tarta en R"](https://soka.gitlab.io/blog/post/2019-02-14-diagramas-de-tarta-en-r/).
- ["R - Gráficos básicos: Función barplot()"](https://soka.gitlab.io/blog/post/2019-03-19-r-graficos-basicos-barplot/).
- ["Diagramas de barras en R"](https://soka.gitlab.io/blog/post/2019-02-21-diagramas-barras-en-r/).
- ["Histogramas con hist()"](https://soka.gitlab.io/blog/post/2019-03-25-r-graficos-basicos-hist/).

[Resumen de todos los posts por categoría de **R** en el blog](https://soka.gitlab.io/blog/tags/r/).

## Diagramas avanzados con gglot2

- ["Gráficos avanzados en R - Introducción a ggplot2"](https://soka.gitlab.io/blog/post/2019-03-25-r-graficos-avanzados-ggplot2-1/).
- ["Guía avanzada Scatter Plots en R con ggplot"](https://soka.gitlab.io/blog/post/2019-04-25-r-ggplot2-scatterplot/).
- ["geom_line() - Gráficos de líneas"](https://soka.gitlab.io/blog/post/2019-04-01-r-ggplot2-grafico-lineas/).
- ["geom_bar() - Gráficos de barras"](https://soka.gitlab.io/blog/post/2019-04-08-r-ggplot2-grafico-barras/).
- ["Histogramas - Introducción a ggplot2"](https://soka.gitlab.io/blog/post/2019-04-08-r-ggplot2-histograma/).
- ["Diagramas de caja"](https://soka.gitlab.io/blog/post/2019-04-11-r-ggplot2-boxplot/).

## General

- ["Repaso rápido conceptos básicos en R"](https://soka.gitlab.io/blog/post/2019-08-10-r-fundamentos/).
- ["Pipes en R"](https://soka.gitlab.io/blog/post/2019-10-05-r-pipe-function/): Pipes con magrittr `%>`, PipeR...
- ["Tres formas de renombrar columnas en R"](https://soka.gitlab.io/blog/post/2019-10-21-r-renombrar-columnas/).

## Probabilidad y estadística

- ["Correlación y regresión en R"](https://soka.gitlab.io/blog/post/2019-10-12-r-correlacion-y-regresion/)

## R Markdown

- ["Publicación de contenidos con R Markdown - Introducción"](https://soka.gitlab.io/blog/post/2019-04-12-publicacion-de-resultados-con-r-markdown-intro/).

## APIs

- ["R Acceso a APIs"](https://soka.gitlab.io/blog/post/2019-05-18-r-acceso-apis/): Introducción básica usando jsonlite y httr.
- ["API pública de Twitter"](https://soka.gitlab.io/blog/post/2019-04-24-r-twitter/).
- ["Crear una RESTful API con R con plumber"](https://soka.gitlab.io/blog/post/2019-05-19-r-creando-apis-con-plumber/).

## dplyr

- ["R - paquete dplyr"](https://soka.gitlab.io/blog/post/2019-07-11-r-dplyr-intro/): Introducción básica.

## Shiny

- ["Aplicaciones R para la Web "](https://soka.gitlab.io/blog/post/2019-08-16-r-shiny/): Introducción a Shiny.

## Tidyverse

- ["Introducción a Tidyverse"](https://soka.gitlab.io/blog/post/2019-10-06-r-tidyverse-intro/).

## Árboles decisión

-["Arboles de decisión"](https://soka.gitlab.io/blog/post/2019-08-26-r-arboles-de-decision/): Tomado de "R para profesionales de los datos" de Carlos J. Gil Bellosta. Con paquete [party](https://cran.r-project.org/web/packages/party/index.html).

## Paquetes

- [sctyner/memer](https://soka.gitlab.io/blog/post/2019-09-05-r-sctyner-memer-generador-memes/): Generador de memes para R.
- Lubridate

## Recursos

- ["Recetas para hacer diagramas en R como lo hace la BBC"](https://soka.gitlab.io/blog/post/2019-02-19-graficos-r-estilo-bbc/): **SIN ACABAR**.
- ["Marvel developer API - Consulta desde R tus datos preferidos del universo Marvel Comics"](https://soka.gitlab.io/blog/post/2019-04-27-r-marvel-developer-api/).

**Bibliografía:**

- ["Introducción a R con datos del CIS (I): ¿Qué es R y para qué sirve?"](https://polikracia.com/introduccion-a-r-con-datos-del-cis-i/).
- ["Introducción a R con datos del CIS (II): Visualización con ggplot2"](https://polikracia.com/introduccion-r-datos-cis-ggplot2/).
- [https://github.com/rstudio/cheatsheets/raw/master/rstudio-ide.pdf](https://github.com/rstudio/cheatsheets/raw/master/rstudio-ide.pdf): Cheat Sheet para IDE RStudio.
- ["R for Data Science"](https://r4ds.had.co.nz/): This book will teach you how to do data science with R: You’ll learn how to get your data into R, get it into the most useful structure, transform it, visualise it and model it.
- The [bookdown](https://bookdown.org/) package is an open-source R package that facilitates writing books and long-form articles/reports with R Markdown. ["bookdown: Authoring Books and Technical Documents with R Markdown"](https://bookdown.org/yihui/bookdown/).
- ["Efficient R programming"](https://bookdown.org/csgillespie/efficientR/): This is the online version of the O’Reilly book: Efficient R programming. Pull requests and general comments are welcome.
- ["Practical Data Science with R, Second Edition"](https://www.manning.com/books/practical-data-science-with-r-second-edition): Algunos capítulos disponibles.
- ["Introduction to econometrics with R"](https://www.econometrics-with-r.org/index.html). [GitHub](https://github.com/mca91/EconometricsWithR).
- ["Data Visualization - A practical introduction"](https://socviz.co/index.html) - Kieran Healy.
- ["BBC Visual and Data Journalism cookbook for R graphics"](https://bbc.github.io/rcookbook/).
- ["Another 10 Free Must-Read Books for Machine Learning and Data Science"](https://www.kdnuggets.com/2019/03/another-10-free-must-read-books-for-machine-learning-and-data-science.html).
- ["Happy Git and GitHub for the useR"](https://happygitwithr.com/) - Jenny Bryan, the STAT 545 TAs, Jim Hester.
- ["R FOR JOURNALISTS"](https://learn.r-journalism.com/en/).
- ["R Markdown: The Definitive Guide"](https://bookdown.org/yihui/rmarkdown/). Yihui Xie, J. J. Allaire, Garrett Grolemund - 2019-03-19.
- ["Network visualization with R"](http://kateto.net/network-visualization).
- GitHub matloff/fasteR ["fasteR: Fast Lane to Learning R!"](https://github.com/matloff/fasteR/blob/master/README.md).
- saghirb/ [Rmarkdown-Intro-Workshop](https://github.com/saghirb/Rmarkdown-Intro-Workshop/blob/master/README.md).

# Otras herramientas y análisis de datos - DataJournalism

Windows Subsystem for Linux, or [WSL](https://www.computerhope.com/jargon/w/wsl.htm), is an optional feature of Windows 10.

Curso [**Bases para el periodismo de datos**](https://flowsta.github.io/bases-periodismo-datos/) . Author: Adolfo Antón Bravo.

[https://datajournalism.tools/](https://datajournalism.tools/).

[**csvkit**](https://csvkit.readthedocs.io/en/latest/index.html#) (Python) is a suite of command-line tools for converting to and working with CSV, the king of tabular file formats.

- [Tutorial](https://csvkit.readthedocs.io/en/latest/tutorial.html).
- Documentation: http://csvkit.rtfd.org/
- Repository: https://github.com/wireservice/csvkit
- Issues: https://github.com/wireservice/csvkit/issues
- Schemas: https://github.com/wireservice/ffs

[**OpenRefine**](http://openrefine.org/) (formerly Google Refine) is a powerful tool for working with messy data: cleaning it; transforming it from one format into another; and extending it with web services and external data.

[**VisiData**](https://visidata.org/) is an interactive multitool for tabular data. It combines the clarity of a spreadsheet, the efficiency of the terminal, and the power of Python, into a lightweight utility which can handle millions of rows with ease.

["An Introduction to VisiData"](https://jsvine.github.io/intro-to-visidata/) VisiData is a free, open-source tool that lets you quickly open, explore, summarize, and analyze datasets in your computer’s terminal. VisiData works with CSV files, Excel spreadsheets, SQL databases, and many other data sources.

[http://databin.pudo.org/](http://databin.pudo.org/): databin helps you to share tabular data — a few rows from Excel or a result from a SQL prompt — with others.

[https://app.datawrapper.de/](https://app.datawrapper.de/):

[http://workbenchdata.com/](http://workbenchdata.com/): Create and share reproducible data workflows, without code.

[https://pixelmap.amcharts.com/](https://pixelmap.amcharts.com/).

[https://www.databasic.io/en/wordcounter/](https://www.databasic.io/en/wordcounter/) Analyse texts by counting and highlighting the most common words and phrases with Wordcounter.

[https://www.databasic.io/en/wtfcsv/](https://www.databasic.io/en/wtfcsv/) Find patterns in your data by exploring and analysing your data with WTFcsv. Upload your data in a CSV file.

[PageOneX](http://pageonex.com/): Visualize the evolution of stories on newspaper front pages. Herramienta Web que permite seleccionar una Web un periodo de tiempo y unos tópicos a analizar. Permite exportar los resultados a formato .ods. [Documentación](https://github.com/montera34/pageonex/wiki).

[Twitonomy][(http://www.twitonomy.com/)]: Twitter #analytics and much more...

[web.archive.org](https://web.archive.org/): Info webs antiguas o cerradas?

["La Wayback Machine ya compara las páginas que mantiene en su archivo histórico (en beta)"](https://www.microsiervos.com/archivo/internet/wayback-machine-compara-paginas-archivo-historico-web.html).

[https://trackography.org/](https://trackography.org/): Find out who is tracking you when you are reading your favourite news online.

GitHub [Tabletop.js](https://github.com/jsoma/tabletop/blob/master/README.md): JS que convierte enlace público Google SpreadSheet en JSON.

["7 Command-Line Tools for Data Science"](https://www.datascienceworkshops.com/blog/seven-command-line-tools-for-data-science/): Obtain, scrub, and explore data with jq, json2csv, csvkit, scrape, xml2json, sample, and Rio.

[https://mapshaper.org/](https://mapshaper.org/): Tools for editing Shapefile, GeoJSON, TopoJSON and CSV files. [https://github.com/mbloch/mapshaper](https://github.com/mbloch/mapshaper).

# Instalación inicial

## Interprete R-3.5.1-win.exe

Web oficial del proyecto [R: The R Project for Statistical Computing](https://www.r-project.org/).

CRAN (Comprehensive R Archive Network) [Mirrors](https://cran.r-project.org/mirrors.html).

España [https://ftp.cixug.es/CRAN/](https://ftp.cixug.es/CRAN/) [Download R for Windows](https://ftp.cixug.es/CRAN/bin/windows/).

## IDE RStudio-1.1.453.exe Desktop Free

Web: [RStudio](https://www.rstudio.com/products/RStudio/)

# Paquetes

Podemos comprobar paquetes instalados (que no cargados) con el siguiente comando:

```
library()
```

El resultado se muestra en una nueva pestaña, en mi caso trabajando con MS Win y la versión de R instalada los paquetes instalados de alojan en "C:/Program Files/R/R-3.5.1/library’".

![](images/05.PNG)

Usando el IDE RStudio también podemos comprobarlo en una de las pestañas.

![](images/02.PNG)

Instalación de paquetes por consola:

```
install.packages("nombre_del_paquete",dependiencies=TRUE)
```

Podemos igualmente usar el IDE de RStudio.

![](images/03.PNG)

Carga de paquetes desde consola:

```
library("nombre_del_paquete")
```

En RStudio sólo debemos marcar el tick a la izquierda de cada paquete.

Desinstalación de paquetes:

```
remove.packages("nombre_del_paquete")
```

# Ayuda

```
help("nombre_de_funcion")
??nombre_de_funcion
```

Ejemplo `help("library")`:

![](images/04.PNG)

# Aprendiendo R

Existe una librería interactiva en modo consola para empezar con R:

```
install.packages("swirl", dependencies = TRUE)
library("swirl")
```
