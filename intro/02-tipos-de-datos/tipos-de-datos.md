Tipos de datos básicos en R:

* Enteros
* Numéricos
* Complejos
* Lógicos
* Cadenas de texto

# Enteros

Números sin decimales, por defecto R asigna los números como númericos, para que sea un entero debemos hacer un "casting" para que sea entero.

```
f = 3
class(f)
f = as.integer(f)
```

![](intro/01-tipos-de-datos/images/01.PNG)


# Numéricos

Números con decimales. 

```
> x = 10.5       # assign a decimal value 
> x              # print the value of x 
[1] 10.5 
> class(x)       # print the class name of x 
[1] "numeric"
```

Como hemos visto si asignamos un entero a la variable seguirá almacenandolo como tipo numérico.

```
> x = 7
> is.integer(x)  	# is x an integer? 
[1] FALSE
```

# Complejos

```
> a = 1 + 2i
> a
[1] 1+2i
```

# Lógicos

```
> x = 1; y = 2
> z = x > y
> z
[1] FALSEs
```

# Cadenas de texto

Permiten almacenar caracteres. Cualquier tipo, puede convertirse en cadena de texto. Cualquier valor rodeado de comillas simples o dobles es tratado como una cadena de texto o string en R (aunque definas la cadena con comillas simples R internamente siempre las almacena con comillas dobles).

```
> a = 'Start and end with single quote'
> print(a)
[1] "Start and end with single quote"

> b = "Start and end with double quotes"
> print(b)

> c = "single quote ' in between double quotes"
> print(c)

> d = 'Double quotes " in between single quote'
> print(d)
```

## Manipulación de cadenas

### Concatenación

Podemos combinar tantas cadenas como queramos usando la función `paste()`:

```
paste(..., sep = " ", collapse = NULL)
```

Donde:

* `...`: Representa tantos argumentos o cadenas como queramos.
* `sep`: Argumento opcional, representa un separador que se añade entre las cadenas combinadas.
* `collapse`: Es usado para eliminar los espacios en blanco entre las cadenas (pero no entre las palabras de cada cadena).

```
> a = "Hello"
> b = 'How'
> c = "are you? "
> print(paste(a,b,c))
[1] "Hello How are you? "
> print(paste(a,b,c, sep = "-"))
[1] "Hello-How-are you? "
> print(paste(a,b,c, sep = "", collapse = "")
[1] "HelloHoware you?
```


# Enlaces externos

* [Numeric | R Tutorial](http://www.r-tutor.com/r-introduction/basic-data-types/numeric): A discussion of the numeric data type in R.
* [R - Strings - Tutorials Point](https://www.tutorialspoint.com/r/r_strings.htm).