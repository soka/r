# Fuente: https://rfortherestofus.com/2019/08/themes-to-improve-your-ggplot-figures/

# --------------------------------------------------------
# hrbrthemes
# --------------------------------------------------------

# https://cran.r-project.org/web/packages/hrbrthemes/index.html

if (!require(hrbrthemes)) {remotes::install_gitlab("hrbrmstr/hrbrthemes")}
if (!require(gcookbook)) {install.packages("gcookbook")}
if (!require(tidyverse)) {install.packages("tidyverse")} 

library(hrbrthemes)
library(gcookbook)
library(tidyverse)

packageVersion("hrbrthemes")

# Base theme (Arial Narrow)
ggplot(data = mpg) +
  geom_point(mapping = aes(x = displ,y = hwy,colour=class)) + 
  labs(x="Fuel efficiency (mpg)", y="Weight (tons)",
       title="Modern Roboto Condensed typography",
       subtitle="A plot that is only useful for demonstration purposes",
       caption="Brought to you by the letter 'g'") + 
  #theme_ipsum() + scale_color_ipsum() 
  #theme_ipsum_rc() # Roboto Condensed
  #theme_ft_rc() # New FT Theme!
  theme_ft_rc() +
  scale_color_ft()

# ----------------------------------------------------------------------
# firatheme
# ----------------------------------------------------------------------
if (!require(firatheme)) {devtools::install_github("vankesteren/firatheme")}
if (!require(ggplot2)) {install.packages("ggplot2")}
library(ggplot2)
library(firatheme)

ggplot(data = mpg) +
  geom_point(mapping = aes(x = displ,y = hwy,colour=class)) + 
  labs(x="Fuel efficiency (mpg)", y="Weight (tons)",
       title="firatheme is based on Fira Sans",
       subtitle="A plot that is only useful for demonstration purposes",
       caption="Brought to you by the letter 'g'") + 
  theme_fira() +
  scale_colour_fira()

firaPalette(n = 25)
