# Comandos

Los comandos en R se separan entre sí como en muchos lenguajes de programación usando ";" o si los separamos en nuevas líneas. 

```
a <- 3; b <- 4
# Es lo mismo que
a <- 3
b <- 4
```

El prompt del entorno donde introducimos los comandos es `>` , cuando no completamos un comando el sistema lo indica con el carácter `+`.

```
> sum(4,5
+ )
[1] 9
```

# Expresiones 

Una expresión es un comando que R debe evaluar, ejecutar y producir un resultado.

Ejemplo

```
> 7 + sum(3,-4)
[1] 6
```

# Operador asignación

Cuando se trata de asignar un valor a una variable en R podemos usar indistintamente `<-` o `=`. 

```
> MyVal1 = 7 + 3 
> MyVal2 <- 7 + 3 
```

* r-bloggers.com ["Difference between assignment operators in R"](https://www.r-bloggers.com/difference-between-assignment-operators-in-r/).
* Ayuda `help("assignOps")` 


# Operadores 

**Un operador es una función** que realiza una operación (valga la redundancia) sobre uno o más objetos. 

## Aritméticos

Podemos acceder a la ayuda sobre operadores aritméticos con `help("+")`. 

* `+`: Adición o suma. No solo puede sumar dos números, tambien se puede aplicar a la suma vectores.

```
> x <- c(1,2,3)
> y <- c(1,2,3)
> x+y
[1] 2 4 6
```

* `-`: Resta. 
* `*`: Multiplicación. 
* `/`: División.
* `^`: Elevado a la potencia.
* `%%`: Resto. 7 %% 3 = 1.

## Lógicos

Operadores lógicos o de comparación: `<`, `<=`, `>`, `>=`, `==`, `!=`, `!x`, `x|y`, `x&y`, `isTRUE(x)`

Un ejemplo:

```
> x <- c(1:10)
> x[(x>8) | (x<5)]
[1]  1  2  3  4  9 10
```

# Referencias externas 
 
* statmethods.net ["Operators"](https://www.statmethods.net/management/operators.html).
