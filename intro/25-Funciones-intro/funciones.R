# https://soka.gitlab.io/blog/post/2019-03-15-r-funciones-intro/
# Iker Landajuela 

# ##########################################################
# 
# FUNCIÓN BÁSICA
#
# ##########################################################

# INPUT -> [CAJA NEGRA] -> OUTPUT

# ----------------------------------------------------------

Func1 <- function(edad) {
  edad
}

Func1(30)
Func1(40)

# ----------------------------------------------------------

# Función para calcular el cuadrado de un nÃºmero.
cuadrado <- function (x) {
  return (x^2)
}

cuadrado(2)

# El orden no importa si ponemos el nombre del parámetro
f <- function(a,b) {
  cat("a es: ",a," y b es:",b)
}

f(b=3,a=1)


# ----------------------------------------------------------

Func2 <- function(x,y) {
  x+y
}

Func2(1,2)

Func2(1) # Error

mi_suma <- function(x,y=1) {
  x+y
}

mi_suma(3)

args(mi_suma)

# Variable global
mi_suma <- function(x,y=1) {
  x+y+numero
}
mi_suma(2,3) # Error
numero <- 5 # Variable global 
mi_suma(2,3) # ok = 10

# ##########################################################
# 
# RETORNO DE FUNCIONES 
# 
# ##########################################################

Func3 <- function(x,y) {
  return(x+y)
}
z <- Func3(1,2)

# Como no hemos puesto restricciones sobre los argumentos se pueden sumar objetos
# de cualquier tipo, por ejemplo vectores o matrices
Func3(c(1,2,3),c(1,2,3))

# -----------------------------------------------------------

# Funcion que calcula la hipotenusa de un triángulo
hipotenusa <-  function (x,y) {
  z = sqrt(x^2+y^2)
  return (z)
}

hipotenusa(1,3)

# -----------------------------------------------------------

# Función que calcula el área de un cuadrado
area <- function (x,y) {
  z = x * y
  return (z)
}


# ##########################################################
# 
# DEVOLVER MÁS DE UN PARÁMETRO
# 
# ##########################################################

Func4 <- function() {
  x <- 1
  y <- 2
  return(c(x,y))
}

# ##########################################################
# 
# UTILIDADES PARA FUNCIONES
# 
# ##########################################################
# Esta función permite establecer o obtener los argumentos de una función
formals(Func3)

body(Func3)

# Para editar la función 
edit(Func3)

# Controlar parámetros no definidos
func <- function(a, b) {
  if (missing(a)) {
    cat("missing")
  }
}

func()
func(1,2)

# Muestra los nombres de los argumentos de una función y sus valores 
# por defecto
args(sd)

# Desviación estándar
?sd
v <- c(1,3,5,NA)
# Retorna NA porque no puede calcular desviación estándar si faltan valores
sd(v)
sd(x=v,na.rm = T)
# Equivalente
sd(v,T)
sd(na.rm = T,x=v)
