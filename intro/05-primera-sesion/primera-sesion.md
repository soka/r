# Nuestra primera sesión en R

## Hola Mundo

Todos los tutoriales de lenguajes de programación comienzan escribiendo la celebra frase "Hello World!" por pantalla. 

En R lo hacemos con esta línea:

```
> print("Hello World!")
```

El interprete de comandos de R evalua la sentencia y muestra la salida inmediatamente:

```
[1] "Hello World!"
```

![](intro/05-primera-sesion/images/01.png)

## Matemáticas sencillas

Escribe lo siguiente en la consola para calcular una sencilla suma: 

```
> 1 + 2 + 3 + 4 + 5
[1] 15
```

# Usando vectores

Un vector es una colección de objetos, una colección de números es un vector numérico por ejemplo.

```
> c(1, 2, 3, 4, 5)
[1] 1 2 3 4 5
```

Para construir un vector hemos usado una función (ver `> help("c")`).  También se puede construir un vector usando operadores, uno muy útil es del operador de secuencia `:`:

```
> x <- c(1:5)
> sum(x)
[1] 15
```

# Almacenando valores

Podemos almacenar los datos en variables para su posterior tratamiento en otros puntos del programa, en sólo dos línea hemos creado una secuencia, almacenada en una variable llamada "x" y hemos impreso su contenido por consola:

```
> x <- 1:5
> x
[1] 1 2 3 4 5
```

Podemos hacer cálculos con la variable, en el siguiente ejemplo si sumamos un número a un vector se suma a cada uno de sus elementos:

```
> x <- 1:5
> y <- x + 10
> y
[1] 11 12 13 14 15
```

Las variables también pueden contener cadenas de texto:

```
> cadena <- "Hello" 
> cadena
[1] "Hello"
```

O un vector formado por cadenas de texto:

```
> str <- c("Hello"," World!")
> str
[1] "Hello"   " World!"
```

# Interacción con el usuario

Para obtener un dato del usuario podemos la función `readline()` como sigue:

```
> y <- readline("Deme su nombre:")
Deme su nombre:Iker
> y
[1] "Iker"
```




















