El manejo de vectores es una de las claves de R, un vector unidemensional de valores, todos del mismo tipo (un solo valor es tecnicamente un vector también).

# Inspeccionando un vector

Con la función `str()` podemos inspeccionar la **estructura de una variable**:

```
> x <- c(1:5)
> str(x)
 int [1:5] 1 2 3 4 5
```

# Longitud de un vector

Para conocer la **longitud de un vector** usamos la función `length()`

```
> x <- c(1:5)
> length(x)
[1] 5
```

# Comprobando el tipo de datos de un vector

```
> myVector <- c(1:5)
> is.numeric(myVector)
[1] TRUE
> is.integer(myVector)
[1] TRUE
> StrVector <- c("Iker","Landajuela")
> is.integer(StrVector)
[1] FALSE
```

# Creando un vector

Para crear un vector basado en una secuencia ordenada de enteros usamos el operador `:`.

Por ejemplo:

```
> c(3:7)
[1] 3 4 5 6 7
```

Crea una secuencia del 3 al 7 ambos incluidos. Si hacemos lo siguiente:

```
> c(4:-3)
[1]  4  3  2  1  0 -1 -2 -3
```

La secuencia es decreciente.

Si queremos que los intervalos del salto sean más detallados usamos la función `seq()`.

```
> seq(from = 4.5, to = 2.5, by = -0.5)
[1] 4.5 4.0 3.5 3.0 2.5
```

# Combinando vectores

La función `c()` se refiere a _combine_ , veamos un ejemplo:

```
> vector.fruta <- c("naranja","manzana","pera")
> vector.color <- c("verde","rojo")
> c(vector.fruta,vector.color)
[1] "naranja" "manzana" "pera"    "verde"   "rojo"
```

La combinación mantiene el orden de los vectores.

Podemos combinar el mismo vector tantas veces como queramos para repetir sus contenidos.

# Repitiendo vectores

Usamos la función `rep()`:

```
> rep(c(1:3),times = 3)
[1] 1 2 3 1 2 3 1 2 3
```

También podemos repetir cada valor tantas veces como queramos:

```
> rep(c(1:3),each = 3)
[1] 1 1 1 2 2 2 3 3 3
```

# Trabajando con los valores de un vector

Cada vez que R muestra el contenido de un vector lo precede con `[]` por delante. El número entre corchetes indica la posición del índice

```
> myVector <- c(1:30)
> myVector
 [1]  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25
[26] 26 27 28 29 30
```

Si creamos un vector un poco largo y jugamos con la redimensión del tamaño de consola vemos como el índice de la segunda línea varía.

## Obteniendo valores de un vector

Los corchetes `[ ]` representan una función que permite extraer un valor de un vector. Por ejemplo para obtener la segunda posición del vector de arriba usamos `[2]`.

```
> myVector[2]
[1] 2
```

Esto es bastante comun en muchos lenguajes de programación, algo más impresionante es pasarle un vector a la función para obtener un rango de valores:

```
> myIndex <- c(1:5)
> myVector[myIndex]
[1] 1 2 3 4 5
```

R retorna un vector con las posiciones solicitadas.

Podemos usar el índice para excluir un valor de la salida con el signo negativo.

```
> myVector[-2]
[1]  1  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30
```

También podemos usar simbolos negativos con un índice en forma de vector:

```
> myVector[-(1:20)]
[1] 21 22 23 24 25 26 27 28 29 30
```

Ojo con las parentésis para definir la secuencia.

## Modificando los valores dentro del vector

Continuando con el ejemplo de arriba:

```
> myVector <- (1:5)
> myVector[1] <- 200
> myVector
[1] 200   2   3   4   5
```

## Trabajando con vectores lógicos (TRUE, FALSE)

En otros lenguajes los tipos lógicos se denominan booleanos.

## Comparando valores

```
> myVector <- c(1:5)
> myVector
[1] 1 2 3 4 5
> myVector > 2
[1] FALSE FALSE  TRUE  TRUE  TRUE
> which(myVector > 2)
[1] 3 4 5
```

Si queremos obtener los valores que cumplan una condición lógica:

```R
> vNum <- c(1,2,4,10,5,6)
> # Con esto obtenemos los valores que cumplen la condición
> vNum[vNum>2]
[1]  4 10  5  6
```

# Vectores dentro de vectores

Vectores dentro de vectores, el resultado es un nuevo vector `num[1:12]`:

```R
> x <- c(1,2,3,4,5)
> y <- c(x,6,7,-x)
> y
 [1]  1  2  3  4  5  6  7 -1 -2 -3 -4 -5
```

## Operaciones vectoriales

Produce un _warning_ pero hace lo que buenamente puede:

```R
> x <- c(1,2)
> y <- c(3,4,5)
> z <- x*y
Warning message:
In x * y : longer object length is not a multiple of shorter object length
> z
[1] 3 8 5
```

Suma 1 a todos los valores del vector:

```R
> x <- c(1,2,4)
> x <- x + 1
> x
[1] 2 3 5
```
