---
title: "Introducción a R Markdown"
author: "Iker Landajuela"
date: "12 de abril de 2019"
output:
  html_document:
    df_print: paged
---

# Título nivel 1
## Título nivel 2


```{r ,echo=FALSE, warning=FALSE}
library("ggplot2")

head(mpg)


ggplot(mpg,aes(x=class,y=hwy)) + 
  geom_boxplot(aes(color=class)) + 
  ggtitle("Consumición por clase")
```` 

**Negrita**

_Cursiva_ o *Cursiva* 


[R Mardown PDF Reference](https://www.rstudio.com/wp-content/uploads/2015/03/rmarkdown-reference.pdf)
