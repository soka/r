year <- 2016
as.Date(sprintf("%d-01-01", year)) + 255

# ------------------------------------------------------------

today <- Sys.time()
doy <- strftime(today, format = "%j")
doy_num <- as.numeric(doy)

library(dplyr)

# operador pipe %>% https://rsanchezs.gitbooks.io/rprogramming/content/chapter9/pipeline.html
doy_num <- Sys.time() %>% strftime(, format = "%j") %>% as.numeric()

if ( Sys.time() %>% strftime(, format = "%j") %>% as.numeric() == 0x100) {
  "Happy programmers day"
} else {
  "Not today"
}

# ------------------------------------------------------------
library(lubridate)

today <- Sys.time()
yday(today)

Sys.time() %>% yday()
