#install.packages(ggplot2)
library("ggplot2")

?mpg
head(mpg)

# Distribución de consumo (hwy) por tipo de coche (class)
ggplot(mpg,aes(x=class,y=hwy)) + 
  geom_boxplot(aes(color=class)) + 
  ggtitle("Consumición por clase")
