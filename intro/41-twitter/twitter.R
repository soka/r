# ################################################################
#
# API TWITTER
#
# ################################################################
# Iker Landajuela
# https://soka.gitlab.io/blog/post/2019-04-24-r-twitter/


# https://www.rdocumentation.org/packages/twitteR


install.packages("twitteR")
library(twitteR)

setwd("~/GitLab/r/intro/41-twitter")
source("twitter-keys.R")


setup_twitter_oauth(consumer_key,consumer_secret,access_token,access_secret)

getCurRateLimitInfo()


resource_families

getCurRateLimitInfo(c("users","followers","account"))


# #################################################################
#
# USUARIO
#
# #################################################################
usuario <- getUser('ILandajuela')
usuarioDF <- as.data.frame(usuario)
View(usuarioDF)

usuarioDF$id
usuarioDF$screenName

names(usuarioDF)

usuarioDF <- as.data.frame(getUser(1120954979651657728))

# #################################################################
#
# B�SQUEDA POR HASHTAG
#
# #################################################################
# B�squeda de terminos con hashtag
rdmTweets <- searchTwitter('#28A ', n=500,lang = 'es')
class(rdmTweets)
View(rdmTweets)
# Error 
# as.data.frame(rdmTweets)
tweetsDF <- twListToDF(rdmTweets)

# #################################################################
#
# TRENDS
#
# #################################################################
td <- getTrends(754542)

