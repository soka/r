# Usamos un dataset que viene con R llamado iris
library(help = "datasets") # Para consultar datasets disponibles
head(iris) # Muestra solo los primeros registros 
class(iris)
str(iris)

# Seleccionamos un elemento determinado Fila 1 Columna 2
iris[1,2] 
# Filas 1 y 2, Columna 2
iris[1:2,2] 
iris[1:2,1:2] # Funciona tambi�n 
iris[c(2,4,6),2]

# Seleccionamos una columna
iris["Sepal.Width"]
iris$Sepal.Width

# Seleccionamos varias columnas con un vector
iris[c("Sepal.Width","Sepal.Length")]

# --------------------- subset() ---------------------------------
# Ahora haremos lo mismo usando subset(), retorna un subconjunto de datos de vectores, 
# matrices o data frames que cumplan unas determinadas condiciones
iris1 <- subset(iris,iris$Sepal.Width > 3 & iris$Species == 'setosa')
dim(iris1)

# ----------------- Listas ----------------------------
# Con listas
v1 <- c(1,2,3)
v2 <- c("a","b","c")
v3 <- c(TRUE,FALSE,TRUE)
l1 <- list(v1,v2,v3)
# Primer objeto de la lista
l1[[1]] 
l1[1,2] # Esto NO funciona
l1[1:2]
# Del primer vector 2. posici�n
l1[[1]][2]
l1[[1]][1:2]
