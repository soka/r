# Guía de estilo en R de Google

No existe un estándar o un consenso común sobre el estilo de programación en R. Una buena referencia es la que ofrece Google en la siguiente dirección [https://google.github.io/styleguide/Rguide.xml](https://google.github.io/styleguide/Rguide.xml).

![](intro/03-guia-de-estilo/images/01.PNG)

En general recomiendan la sintaxis [CamelCase](https://en.wikipedia.org/wiki/Camel_case) con la primera letra siempre em mayuscula,  si son varias palabras podemos separlas con el guión bajo <<_>>, etc...

Las extensiones de los ficheros de script deberían tener la extensión <<.R>>. 

En varios capítulos bastante rápidos de consultar la guía ofrece unos criterios básicos. Llama la atención que para la identación de código use dos espacios y no el tabulador (la vieja batalla desde que el código es código), para abrir bloques de código con <<{>> también sugiere no usar el carácter en su propia línea, para cerrar el código con <<}>> sí lo podemos usar en su propia línea.

```
if (condition) {
  # one or more lines
} else {
  # one or more lines
}
```

También realiza unas recomendaciones sobre como [comentar el código](https://google.github.io/styleguide/Rguide.xml#comments). 