library("ggplot2")

ggplot(data = iris,aes(x=Sepal.Width)) + geom_histogram()

ggplot(data = iris,aes(x=Sepal.Width)) + geom_histogram(binwidth = 0.2,color="black",fill="steelblue") + 
  ggtitle("Histograma") + 
  xlab("Ancho petalo") +
  ylab("Cantidad")

ggplot(data = iris,aes(x=Sepal.Width)) + 
  geom_histogram(binwidth = 0.2,color="black",fill="steelblue", aes(y=..density..)) + 
  ggtitle("Histograma") + 
  xlab("Ancho petalo") +
  ylab("Cantidad") + 
  geom_density(stat="density",alpha=I(0.2),fill="red")
