# funci�n media mean()
datos <- c(12,40,54,34)
media <- mean(datos)
media

# Suma de valores
sum(datos)

max(datos)

min(datos)

sd(datos)

# Inspeccionando un DF
dfPersonas <- data.frame("SN" = 1:2, "Age" = c(21,15), "Name" = c("John","Dora"))
table(dfPersonas$Age) #
str(dfPersonas) #  2 Observaciones y 3 variables (columnas)
class(dfPersonas) # "data.frame"
summary(dfPersonas) # Resumen m�nima, media, ...
