# Directorio de trabajo

`getwd` retorna una ruta absoluta del directorio de trabajo actual. Retorna una cadena de caractéres o `NULL` si no está disponible. 

```
> myDir = getwd() # Comprobar el directorio de trabajo
> print(myDir)
[1] "C:/Users/IEUser/Documents"
```

Enlaces:

* [getwd function | R Documentation](https://www.rdocumentation.org/packages/base/versions/3.5.0/topics/getwd).

# Limpiar entorno de trabajo

La función `ls` retorna un vector de cadenas de texto la lista de nombres de objetos disponibles en un entorno, cuando se invoca sin argumentos obtiene los data sets y funciones definidas por el usuario. La función `rm` a su vez borra los objetos pasados como parámetros a la función.

```
> rm(list=ls()) # Limpiamos el entorno de trabajo
```

Podemos hacerlo paso a paso:

```
> myNum = 4
> list=ls()
> list
[1] "myNum"
> rm(list)
> list
function (...)  .Primitive("list")
```

Si queremos más detalles del comando siempre podemos recurrir a la función `help()`:

```
> help("ls")
```

Enlaces:
* [ls function | R Documentation](https://www.rdocumentation.org/packages/base/versions/3.5.0/topics/ls).
* [R: Remove Objects from a Specified Environment](https://stat.ethz.ch/R-manual/R-devel/library/base/html/rm.html).

# Limpiar la consola en R-Studio y otros atajos de teclado

Para limpiar la consola simplemente podemos usar el atajo de teclado `CTRL+L`.


# Sistema de ayuda en R

Como decía un profesor de la universidad no es necesario conocer toda la sintaxis de memoria (es más bien imposible conocer todas las funciones y sus parámetros), lo importante es saber donde y como buscar ayuda y tener algunas referencias y recursos importantes a mano siempre. 

## La función <<help()>> o <<?>>

La función `help()` y su equivalente más sencillo `?` permite buscar ayuda sobre un objeto. Vamos a buscar ayuda sobre la propia función `help()` por ejemplo:

```
> ?help
```

En una de las ventanas de R-Studio se mostrará toda la información sobre la función, en la primera línea se muestra la palabra buscada y a continuación a que paquete pertenece. Despues detalla los argumentos, la descripción del comando y al final mi parte preferida con ejemplos de uso.

## Función <<help.search()>>

La función anterior `help()` sólo nos vale si recordamos el nombre exacto, si queremos buscar por palabras podemos usar `help.search()` usando la palabra como argumento entrecomillada.

Pongamos por ejemplo que búscamos sobre representaciones gráficas:

```
> help.search("graph")
```

## La función <<help.start()>>

Este comando muestra la ayuda en HTML, si no se especifican parámetros recurre a un servidor HTTP montado en  nuestra máquina local en la siguiente URL http://127.0.0.1:11744/doc/html/index.html. 

## La función <<example>>

La función `example()` es de gran utilidad cuando vamos a usar una nueva función, muestra ejemplos de uso que podemos reutilizar y nos ayudan a entender su funcionamiento.

Ejemplos de la función aritmética `sum()` que suma los elementos del vector pasado como parámetro:

```
> example("sum")
``` 

Si por ejemplos ejecutamos `example(barplot)` los propios ejemplos cargan gráficas visuales de barras en una de las ventanas de R-Studio. 










